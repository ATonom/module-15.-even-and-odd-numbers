﻿#include <iostream>


void listEvenOrOdd(bool isEven, int number)
{
	if (isEven == true)
	{
		std::cout << "List of even numbers from 0 to " << number << ":";
		std::cout << std::endl;
	}
	else
	{
		std::cout << "List of odd numbers from 0 to " << number << ":";
		std::cout << std::endl;
	}

	for (int i = 1 + (int)isEven; i <= number; i += 2)
	{
		std::cout << i << " ";
	}

	std::cout << std::endl;
}

int main()
{
	const int number = 45;


	std::cout << "List of even numbers from 0 to " << number << ":";
	std::cout << std::endl;

	for (int i = 2; i <= number; i += 2)
	{
		std::cout << i << " ";
	}

	std::cout << std::endl;
	listEvenOrOdd(true, number);
	listEvenOrOdd(false, number);
}


/*
* В главном исполняемом файле(файл в котором находится функция main)
* используя цикл и условия вывести в консоль все четные числа от 0 до N
* (N задавать константой в начале программы).
* 
* Написать функцию, которая в зависимости от своих параметров печатает в
* консоль либо четные, либо нечетные числа от 0 до N(N тоже сделать параметром функции).
* Минимизировать количество циклов и условий.
*/

